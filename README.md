# vagrant boxes

This is a simple packer configuration for generating qemu compatiable
VMs and then creating vagrant boxes.

Thanks to skamithi for creating the initial readme. Check out his blog
for creating [libvirt-vagrant](http://linuxsimba.com/building-qcow-vagrant-box/)
compatible boxes.

Forked from [packer-libvirt-profiles][https://github.com/jtoppins/packer-libvirt-profiles] by jtoppins.

## Requirements
* Install [Libvirt](http://libvirt.org)

## Download Packer

[Packer](http://www.packer.io/intro) is for creating machine images from a
single source configuration.

```
apt install packer
```

## Build a  Libvirt Compatible Box.

```
cd packer-libvirt-profiles
packer build debian-9.4-amd64.json
```
